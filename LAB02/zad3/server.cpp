
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream>
#include <csignal>
#include <string>
#include <bitset>

using namespace std;

size_t buffor = 256;
size_t clientLimit = 1;
int serverDesc = 0;

int  TCPconnection(const string);
int acceptClient(int);
void sigHandler(int);

int main(int argc, char const *argv[]) {
    if (argc != 2) {
        cerr << "Użycie: <PORT>" << endl;
        exit(EXIT_FAILURE);
    }

    serverDesc = TCPconnection(argv[1]);
    signal(SIGINT, sigHandler);
    string msg = "Laboratorium PUS";
    cout << "Serwer działa na porcie " << argv[1] << endl;

    
    while (true) {
        int clientDesc = acceptClient(serverDesc);
        send(clientDesc, msg.data(), msg.size(), 0);
    }

    cout << "Zamykanie programu serwera." << endl;
    close(serverDesc);
    return 0;
}

    void sigHandler(int signal) {
    cout << "Konczenie programu sygnalem: "<< signal << endl;
    close(serverDesc);
    exit(EXIT_SUCCESS);
}

int TCPconnection(const string sPort) {
    serverDesc = socket(AF_INET6, SOCK_STREAM, 0);
    int port = stoi(sPort);

    //Walidacja numeru portu:
    try {
        if (port <= 0 || port >= 65535) {
            throw invalid_argument("Numer portu nie zawiea sie w dozwolonym przedziale.");
        }
        if (port >= 0 && port <= 1024) {
            cout << "Zarezerwowane numery portow! Potrzebujesz roota." << endl;
        }
    } catch (const exception& e) {
        cerr << "Niepoprawnie wpisany numer portu: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }

    if (serverDesc < 0) {
        cerr << "Error while creating creating socket" << endl;
        exit(EXIT_FAILURE);
    }

    sockaddr_in6 ip6Addr = {};
    ip6Addr.sin6_port = htons(port);
    ip6Addr.sin6_family = AF_INET6;
    ip6Addr.sin6_scope_id = 0;
    ip6Addr.sin6_addr = in6addr_any; // przechowuje ::0 jako IPv6 w porzadku Network byte order

    int b = bind(serverDesc, (sockaddr*)&ip6Addr, sizeof(ip6Addr));
    if (b == -1){
        cerr << "Error while binding socket on port: "<< port << endl;
        perror("");
        exit(EXIT_FAILURE);
    }

    int l = listen(serverDesc, clientLimit);
    if (l == -1) {
        perror("Error while listening on socket :");
        exit(EXIT_FAILURE);
    }

    return serverDesc;
}


int acceptClient(int serverDescr) {
    sockaddr_in6 clientAdr = {};
    socklen_t clientAdrLen = sizeof(clientAdr);

    int clientDesc = accept(serverDescr, (sockaddr*)&clientAdr, &clientAdrLen);
    if (clientDesc == -1) {
        perror("Blad podczas akceptacji polaczenia od klienta: ");
        exit(EXIT_FAILURE);
    }

    char clientAddr[128] = {};
    unsigned short clientPort = ntohs(clientAdr.sin6_port);
    bool isMapped = IN6_IS_ADDR_V4MAPPED(&clientAdr.sin6_addr);
    inet_ntop(AF_INET6, &clientAdr.sin6_addr, clientAddr, sizeof(clientAddr));

    
    cout << "Nowe połączenie z : " << (isMapped ? "[IP4-mapped] " : "[IP6] " )
        << clientAddr << ':' << clientPort << endl;
    
    return clientDesc;
}