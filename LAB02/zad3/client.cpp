#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream>
#include <csignal>
#include <string>

using namespace std;

size_t buffor = 256;
int TCPconnection(const char*, string);

int main(int argc, char const *argv[]) {
  if (argc != 3) {
    cerr << "Użycie: <IP> <PORT>" << endl;
    exit(EXIT_FAILURE);
  }

  int socClientDesc = TCPconnection(argv[1], argv[2]);
  char msg[buffor] = {};

  recv(socClientDesc, msg, buffor, 0);
  cout << msg << endl;

  cout << "Zamykanie polaczenia." << endl;
  close(socClientDesc);
  return 0;
}

int TCPconnection(const char* adr, string sPort) {
  
    int socDesc = socket(PF_INET, SOCK_STREAM, 0);
    if (socDesc == -1) {
        cerr << "Blad przy tworzeniu portu." << endl;
        exit(EXIT_FAILURE);
    }

    sockaddr_in ip4Addr = {};
    ip4Addr.sin_family = AF_INET;
    int port = stoi(sPort);

    //Walidacja numeru portu:
    try {
        
        if (port <= 0 || port >= 65535) {
            throw invalid_argument("Numer portu nie zawiea sie w dozwolonym przedziale.");
        }
        if (port >= 0 && port <= 1024) {
            cout << "Zarezerwowane numery portow! Potrzebujesz roota." << endl;
        }
    } catch (const exception& e) {
        cerr << "Niepoprawnie wpisany numer portu: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }
    // Walidacja adresu ipv4:
    if (inet_addr(adr) == INADDR_NONE) {
        cerr << "Niepoprawny adres IPv4." << endl;
        exit(EXIT_FAILURE);
    }
    //Wpisanie  do struktury adresowej i konwersja portu w Network Byte Order (BigEndian)
    ip4Addr.sin_port = htons(port);
    // Konwertuje adres adr na binarny dla interfejsu ipv4 i zamiweszcza w strukturze adresowej
    if (inet_pton(AF_INET, adr, &ip4Addr.sin_addr) != 1) {
        cerr << "Nieporawny adres IP!" << endl;
        exit(EXIT_FAILURE);
    }

    int ret = connect(socDesc, (sockaddr*)&ip4Addr, sizeof(ip4Addr));
    if (ret == -1) {
        perror("Blad polaczenia: ");
        exit(EXIT_FAILURE);
    }
    return socDesc;
}