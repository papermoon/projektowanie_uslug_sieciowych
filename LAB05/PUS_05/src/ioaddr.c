
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <arpa/inet.h>

#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <net/ethernet.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>

#define DOWN "DOWN"
#define UP "UP"


int get_socket();
void validate_input(int, const char *, const char *);
void validate_ipv4(const char *);

void create_alias(const char *, const char *, const char *);
void remove_alias(const char *);


int main(int argc, char const *argv[]) {
    if (!(argc == 5 || argc == 3)) {
        fprintf(stderr, "Invalid input\nUSSAGE:\n\t%s <interface or alias> <ADD> <IPv4> <MASK>\nOR: \n\t%s <interface or alias> <DOWN>\n", argv[0], argv[0]);
        exit(EXIT_FAILURE);
    }

    const char * if_name = argv[1];
    const char * op_type = argv[2];

    if (strlen(if_name) > IF_NAMESIZE) {
        fprintf(stderr, "Interface name too long");
        exit(EXIT_FAILURE);
    }

    if (strcmp(op_type, UP) == 0) {
        if (argc != 5) {
            fprintf(stderr, "Invalid input\nUSSAGE:\n\t%s <interface or alias> <ADD> <IPv4> <MASK>\nOR: \n\t%s <interface or alias> <DOWN>\n", argv[0], argv[0]);
            exit(EXIT_FAILURE);
        }
        create_alias(if_name, argv[3], argv[4]);
    } else if (strcmp(op_type, DOWN) == 0) {
        if (argc != 3) {
            fprintf(stderr, "Invalid input\nUSSAGE:\n\t%s <interface or alias> <ADD> <IPv4> <MASK>\nOR: \n\t%s <interface or alias> <DOWN>\n", argv[0], argv[0]);
            exit(EXIT_FAILURE);
        }
        remove_alias(if_name);
    } else {
        fprintf(stderr, "Interface name too long");
        exit(EXIT_FAILURE);
    }

    return 0;
}

int get_socket() {
    int sockfd = socket(PF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        perror("socket: ");
        exit(EXIT_FAILURE);
    }
    return sockfd;
}

void validate_ipv4(const char * ipv4_addr) {
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipv4_addr, &(sa.sin_addr));

    if (result != 1) {
        fprintf(stderr, "Invalid address: %s", ipv4_addr);
        exit(EXIT_FAILURE);
    }
}


void create_alias(const char * if_name, const char * ipv4_addr, const char * net_mask) {
    int soc_fd = get_socket();

    struct ifreq * ifr = (struct ifreq *)malloc(sizeof(struct ifreq));
    struct sockaddr_in* addr = (struct sockaddr_in*)&ifr->ifr_addr;

    // Pobranie flag
    strcpy(ifr->ifr_name, if_name);
    if(ioctl(soc_fd, SIOCGIFFLAGS, ifr) < 0) {
        perror("ioctl: SIOCGIFFLAGS: ");
        close(soc_fd);
        exit(EXIT_FAILURE);
    }

    // sprawdzenie czy interfejs jest włączony (UP) - wymagane
    if (!!(ifr->ifr_flags & IFF_UP)) {
        printf("[%s] interface is UP\n", if_name);
    } else {
        printf("[%s] interface is DOWN - cannot create alias\n", if_name);
        close(soc_fd);
        exit(EXIT_FAILURE);
    }

    // dodanie nowego adresu dla interfejsu
    strcpy(ifr->ifr_name, if_name);
    addr->sin_family = AF_INET;
    if (inet_pton(AF_INET, ipv4_addr, &addr->sin_addr) != 1) {
        fprintf(stderr, "Invalid ipv4 address: %s", ipv4_addr);
        close(soc_fd);
        exit(EXIT_FAILURE);
    }

    if(ioctl(soc_fd, SIOCSIFADDR, ifr) < 0) {
        perror("ioctl: SIOCSIFADDR: ");
        close(soc_fd);
        exit(EXIT_FAILURE);
    }
    printf("[%s] interface set ipv4 to: %s \n", if_name, ipv4_addr);


    if (inet_pton(AF_INET, net_mask, &addr->sin_addr) != 1) {
        fprintf(stderr, "Invalid mask address: %s", ipv4_addr);
        close(soc_fd);
        exit(EXIT_FAILURE);
    }

    if(ioctl(soc_fd, SIOCSIFNETMASK, ifr) < 0) {
        perror("ioctl: SIOCSIFNETMASK: ");
        close(soc_fd);
        exit(EXIT_FAILURE);
    }
    printf("[%s] interface set netmask to: %s \n", if_name, net_mask);

    close(soc_fd);
}

void remove_alias(const char * if_name) {
    int soc_fd = get_socket();
    struct ifreq * ifr = (struct ifreq *)malloc(sizeof(struct ifreq));

    strcpy(ifr->ifr_name, if_name);
    if(ioctl(soc_fd, SIOCGIFFLAGS, ifr) < 0) {
        perror("ioctl: SIOCGIFFLAGS: ");
        close(soc_fd);
        exit(EXIT_FAILURE);
    }
    ifr->ifr_flags = 0x0;
    if(ioctl(soc_fd, SIOCSIFFLAGS, ifr) < 0) {
        perror("ioctl: SIOCSIFFLAGS: ");
        close(soc_fd);
        exit(EXIT_FAILURE);
    }

    close(soc_fd);
}
