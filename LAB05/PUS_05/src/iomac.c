
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
 
#include <arpa/inet.h>
 
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <net/ethernet.h>
 
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
 
#define MAC_ADDR_LEN 19
 
 
void get_mac(const char * if_name, struct ifreq * ifr, int socfd) {
    strcpy(ifr->ifr_name, if_name);
    if(ioctl(socfd, SIOCGIFHWADDR, ifr) < 0) {
        perror("ioctl: SIOCGIFHWADDR");
        close(socfd);
        exit(EXIT_FAILURE);
    }
}
 
 
void get_mtu(const char * if_name, struct ifreq * ifr, int socfd) {
    if (sizeof(if_name) > sizeof(ifr->ifr_name)) {
        fprintf(stderr, "Invalid input, interface name too long\n");
        exit(EXIT_FAILURE);
    }
    strcpy(ifr->ifr_name, if_name);
 
    if(ioctl(socfd, SIOCGIFMTU, ifr) < 0) {
        perror("ioctl: SIOCGIFHWADDR");
        close(socfd);
        exit(EXIT_FAILURE);
    }
}
 
void set_mac(const char * new_mac, struct ifreq * ifr, int socfd) {
    sscanf(new_mac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
        (unsigned char *)&ifr->ifr_hwaddr.sa_data[0],
        (unsigned char *)&ifr->ifr_hwaddr.sa_data[1],
        (unsigned char *)&ifr->ifr_hwaddr.sa_data[2],
        (unsigned char *)&ifr->ifr_hwaddr.sa_data[3],
        (unsigned char *)&ifr->ifr_hwaddr.sa_data[4],
        (unsigned char *)&ifr->ifr_hwaddr.sa_data[5]
    );
   
    ifr->ifr_hwaddr.sa_family = ARPHRD_ETHER;
 
    if(ioctl(socfd, SIOCSIFHWADDR, ifr) < 0) {
        perror("ioctl: SIOCSIFHWADDR: ");
        close(socfd);
        exit(EXIT_FAILURE);
    }
}
 
void set_mtu(short new_mtu, struct ifreq * ifr, int socfd) {
    ifr->ifr_mtu = new_mtu;
    if(ioctl(socfd, SIOCSIFMTU, ifr) < 0) {
        perror("ioctl: SIOCSIFMTU: ");
        close(socfd);
        exit(EXIT_FAILURE);
    }
}
 
void interface_info(const char * if_name, struct ifreq * ifr_mac_data, struct ifreq * ifr_mtu_data) {
    char mac[MAC_ADDR_LEN];
    int mtu = ifr_mtu_data->ifr_mtu;
    sprintf(mac, "%02x:%02x:%02x:%02x:%02x:%02x",
        ifr_mac_data->ifr_hwaddr.sa_data[0],
        ifr_mac_data->ifr_hwaddr.sa_data[1],
        ifr_mac_data->ifr_hwaddr.sa_data[2],
        ifr_mac_data->ifr_hwaddr.sa_data[3],
        ifr_mac_data->ifr_hwaddr.sa_data[4],
        ifr_mac_data->ifr_hwaddr.sa_data[5]
    );
 
    printf("\t[INTERFACE]: %s\t [MAC]: %s\t [MTU]: %d\n", if_name, mac, mtu);
}
 
int main(int argc, char const *argv[]) {
 
    if (argc != 4) {
        fprintf(stderr, "Invalid input, required params: <interface> <MAC> <MTU>\n");
        exit(EXIT_FAILURE);
    }
 
    const char * if_name = argv[1];
    const char * new_mac = argv[2];
    const int new_mtu = atoi(argv[3]);
 
    struct ifreq * ifr_mac_data = malloc(sizeof(struct ifreq));
    struct ifreq * ifr_mtu_data = malloc(sizeof(struct ifreq));
 
    int sockfd = socket(PF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        perror("socket: ");
        exit(EXIT_FAILURE);
    }
 
    if (sizeof(if_name) > sizeof(ifr_mac_data->ifr_name)) {
        fprintf(stderr, "Invalid input, interface name too long\n");
        exit(EXIT_FAILURE);
    }
 
    if (sizeof(new_mac) > sizeof(ifr_mac_data->ifr_name)) {
        fprintf(stderr, "Invalid input, new interface name too long\n");
        exit(EXIT_FAILURE);
    }
 
    get_mac(if_name, ifr_mac_data, sockfd);
    get_mtu(if_name, ifr_mtu_data, sockfd);
 
    printf("INTERFACE INFO BEFORE CHANGE: \n");
    interface_info(if_name, ifr_mac_data, ifr_mtu_data);
 
    set_mac(new_mac, ifr_mac_data, sockfd);
    set_mtu(new_mtu, ifr_mac_data, sockfd);
 
 
 
    get_mac(if_name, ifr_mac_data, sockfd);
    get_mtu(if_name, ifr_mtu_data, sockfd);
 
    printf("INTERFACE INFO AFTER CHANGE: \n");
    interface_info(if_name, ifr_mac_data, ifr_mtu_data);
 
    free(ifr_mac_data);
    free(ifr_mtu_data);
 
    close(sockfd);
    return 0;
}