#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <string>
#include <cstring>
#include "libpalindrome.h"

using namespace std;


int main(int argc, char const *argv[])
{
    // Struktura przechowujaca informacje adresowe:
    struct sockaddr_in adr_ipv4={};
    adr_ipv4.sin_family = AF_INET;
    adr_ipv4.sin_port = htons(atoi(argv[1]));
    adr_ipv4.sin_addr.s_addr = htonl(INADDR_ANY);
    // deskryptor socketu serwera:
    int serverID;
    // bufor na przesylane tresci; max rozmiar wiadomosci = 256 znakow
    char buffor[256] = {};


    //Tablica do przechowania adresu klienta
    char ip4Addr[16] = {};

    // Struktura wskazujaca na klienta (adres socketa klienta)
    struct sockaddr_in adr_client = {};


    // Walidacja ilosci podanych argumentow:
    if (argc != 2){
        cerr << "Poprawne uzycie programu : ./server.o <port>" << endl;
        exit(EXIT_FAILURE);
    }
    //Walidacja numeru portu:
    try {
        int port = stoi(argv[1]);
        if (port <= 0 || port >= 65535) {
            throw invalid_argument("Numer portu nie zawiea sie w dozwolonym przedziale.");
        }
        if (port >= 0 && port <= 1024) {
            cout << "Zarezerwowane numery portow! Potrzebujesz roota." << endl;
        }

    } catch (const exception& e) {
        cerr << "Niepoprawnie wpisany numer portu: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }


    // Tworzenie gniazda UDP:
    serverID = socket(PF_INET, SOCK_DGRAM, 0);
    if(serverID==-1){
        perror("socket()");
        exit(EXIT_FAILURE);
    }

    // Skojarzenie adresu serwera z gniazdem
    if((bind(serverID, (sockaddr*)&adr_ipv4, (socklen_t)sizeof(adr_ipv4))) < 0) {
        cerr << "Blad polaczenia ze strony serwera" << endl;
        perror("");
        exit(EXIT_FAILURE);
    }
    cout << "Serwer dziala na porcie: " << argv[1] << endl;


    while(true){

        // Odbieranie tresci od klienta
        socklen_t adr_client_len = sizeof(adr_client);
        int bytesReceived = recvfrom(serverID, (void*) buffor, sizeof(buffor), 0, (sockaddr*)&adr_client, &adr_client_len);
        if (bytesReceived == -1) {
            cerr << "Blad odbierania wiadomosci od serwera." << endl;
            exit(EXIT_FAILURE);
        }

        cout << "Otrzymano datagram od klienta: " 
            << inet_ntop(AF_INET, &adr_client.sin_addr, ip4Addr, sizeof(ip4Addr)) << ':'
            << ntohs(adr_client.sin_port)
            << endl << "Wiadomosc: " << buffor 
            << endl << "Rozmiar wiadomosci: " << bytesReceived 
            << endl << endl << flush;


        // Sprawdzenie czy wiadomosc jest palindromem liczbowym
        int isPalindrome = is_palindrome(buffor, bytesReceived);
        // czyszczenie bufora wiadomosci
        memset(buffor, 0,  sizeof(buffor));

        if(isPalindrome==1){
            strcpy(buffor, "Wiadomosc jest palindormem liczbowym.");
        }
        else if(isPalindrome==0){
            strcpy(buffor, "Wiadomosc nie jest palindormem liczbowym.");
        }
        else{
            strcpy(buffor, "Bledna wiadomosc.");
        }


        //Wysylanie wiadomosci do klienta
        int bytesSend = sendto(serverID, (void*) buffor, sizeof(buffor), 0, (sockaddr*)&adr_client, adr_client_len);
        if (bytesSend == -1) {
            cerr << "Blad wysylania wiadomosci do serwera." << endl;
            exit(EXIT_FAILURE);
        }

        // czyszczenie bufora wiadomosci
        memset(buffor, 0,  sizeof(buffor));


    }



    // Zamykanie socketa
    close(serverID);

    cout << "Konczenie pracy serwera." << endl;

    return 0;
}
