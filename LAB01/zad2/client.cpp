#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <string>
#include <cstring>

using namespace std;


int main(int argc, char const *argv[])
{
    // Struktura przechowujaca informacje adresowe:
    struct sockaddr_in adr_ipv4={};
    adr_ipv4.sin_family = AF_INET;
    adr_ipv4.sin_port = htons(atoi(argv[2]));
    inet_pton(AF_INET, argv[1], &adr_ipv4.sin_addr);
    // deskryptor socketu:
    int sockID;
    // bufor na przesylane tresci; max rozmiar wiadomosci = 256 znakow
    char buffor[256] = {};


    // Walidacja ilosci podanych argumentow:
    if (argc != 3){
        cerr << "Poprawne uzycie programu: ./client.o <IPv4 addres> <port>" << endl;
        exit(EXIT_FAILURE);
    }
    //Walidacja numeru portu:
    try {
        int port = stoi(argv[2]);
        if (port <= 0 || port >= 65535) {
            throw invalid_argument("Numer portu nie zawiea sie w dozwolonym przedziale.");
        }
        if (port >= 0 && port <= 1024) {
            cout << "Zarezerwowane numery portow! Potrzebujesz roota." << endl;
        }

    } catch (const exception& e) {
        cerr << "Niepoprawnie wpisany numer portu: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }

    // Walidacja adresu ipv4:
    if (inet_addr(argv[1]) == INADDR_NONE) {
        cerr << "Niepoprawny adres IPv4." << endl;
        exit(EXIT_FAILURE);
    }


    // Tworzenie socketa UDP:
    sockID = socket(PF_INET, SOCK_DGRAM, 0);
    if(sockID==-1){
       perror("socket()");
       exit(EXIT_FAILURE);
    }
    

    // Skojarzenie adresu z gniazdem zdalnym, dzieki czemu uzywamy potem funkcji send()
    if((connect(sockID, (sockaddr*)&adr_ipv4, (socklen_t)sizeof(adr_ipv4)))==-1) {
        cerr << "Blad polaczenia ze strony klienta." << endl;
        exit(EXIT_FAILURE);
    }


    while(true){
    
        //Pobieranie wiadomosci z klawiatury
        cout << "Podaj wiadomosc: "; 
        fgets(buffor, sizeof(buffor), stdin);

        //Wysylanie wiadomosci do serwera
        int bytesSend = send(sockID, (void*) buffor, strlen(buffor), 0);
        if (bytesSend == -1) {
            cerr << "Blad wysylania wiadomosci do serwera." << endl;
            exit(EXIT_FAILURE);
        }


        // Sprawdzenie czy cos bedzie wysylane
        if(strlen(buffor)==1) {
            cout << "Wyslano pusta wiadomosc. Koniec." << endl;
            break;
        }

        // czyszczenie bufora wiadomosci
        memset(buffor, 0,  sizeof(buffor));


        // Odbieranie tresci od serwera
        int bytesReceived = recv(sockID, (void*) buffor, sizeof(buffor), 0);
        if (bytesReceived == -1) {
            cerr << "Blad odbierania wiadomosci od serwera." << endl;
            exit(EXIT_FAILURE);
        }


        // Wyswietlenie wiadomosci otrzymanej od serwera
        cout << "Otrzymana wiadomosc: " << buffor << endl;

        // czyszczenie bufora wiadomosci
        memset(buffor, 0,  sizeof(buffor));

    }

    // Zamykanie socketa
    close(sockID);

    return 0;
}
