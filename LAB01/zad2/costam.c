
/* UNIX headers */
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <csignal>

/* Utility headrs */
#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::cerr;
using std::getline;
using std::string;
using std::invalid_argument;


int CLIENT_FD = 0;
const size_t MSG_BUFFER = 256;

int  makeUdpSocket();
void sendMsg(string&);
void getMsg();
void validateInput(const char*, string);
void sigHandler(int);

int main(int argc, char const *argv[]) {
  signal(SIGINT, sigHandler);
  if (argc != 3) {
    cerr << "Usage: <IP> <PORT>" << endl;
    exit(EXIT_FAILURE);
  }

  validateInput(argv[1], argv[2]);

  const int socFd = makeUdpSocket();
  CLIENT_FD = socFd; // global var for sighandler

  sockaddr_in ip4Addr = {};
  ip4Addr.sin_family = AF_INET;
  ip4Addr.sin_port = htons(atoi(argv[2]));
  inet_pton(AF_INET, argv[1], &ip4Addr.sin_addr);
  socklen_t ip4AddrLen = sizeof(ip4Addr);

  const int retVal = connect(socFd, (sockaddr*)&ip4Addr, ip4AddrLen);

  if (retVal == -1) {
    perror("Connection error: ");
    exit(EXIT_FAILURE);
  }

  string input;
  while (true) {
    getline(cin, input);

    sendMsg(input);
    getMsg();
    if (input.empty()) {
      cout << "FIN" << endl;
      break;
    }
    input.clear();
  }

  close(socFd);
  return 0;
}


void sigHandler(int signal) {
  cout << "\nKilling program with signal: "<< signal << " ..." << endl;
  exit(EXIT_SUCCESS);
}

void validateInput(const char* ip4, string port) {
  
  /* Validate ipv4 addres */
  if (inet_addr(ip4) == INADDR_NONE) {
    cerr << "Wrong IP Address" << endl;
    exit(EXIT_FAILURE);
  }

  /* Validate port nuber */
  try {
    int p = stoi(port);
    if (p <= 0 || p >= 65535) {
      throw invalid_argument("Port number must be between o and 65535!");
    }

    if (p >= 0 && p <= 1024) {
      cout << "Need root permission to lisen on this port" << endl;
    }

  } catch (const std::exception& e) {
    cerr << "Wrong Port number!!: " << e.what() << endl;
    exit(EXIT_FAILURE);
  }
}

int makeUdpSocket() {
  int socFd = socket(PF_INET, SOCK_DGRAM, 0);

  if (socFd < 0) {
    cerr << "Error while creating creating socket" << endl;
    exit(EXIT_FAILURE);
  }

  return socFd;
}

void sendMsg(string& msg) {
  char buff[MSG_BUFFER] = {};
  size_t cp = msg.copy(buff, sizeof(buff));
  buff[cp] = '\0';

  cout << buff << endl;
  const int flags = 0;
  const int bytesSend = send(CLIENT_FD, msg.data(),msg.size(), flags);
  if (bytesSend == -1) {
    perror("Send error: ");
    exit(EXIT_FAILURE);
  }
}

void getMsg() {
  char buff[MSG_BUFFER] = {};

  const int flags = 0;
  const int bytesRecv = recv(CLIENT_FD, buff, sizeof(buff), flags);
  if (bytesRecv == -1) {
    perror("Send error: ");
    exit(EXIT_FAILURE);
  }

  cout << "Message recived: " << buff << endl;
}
