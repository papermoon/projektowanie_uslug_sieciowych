
/* UNIX headers */
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* Utility headrs */
#include <iostream>
#include <csignal>
#include <string>

using namespace std;

int ServerIDToKillOnly = 0;
size_t bufor = 256;
size_t connectionLimit = 10;


void signalHandler(int signal) {
    cout << "Zabijanie serwera: "<< signal << " ..." << endl;
    if (!ServerIDToKillOnly) {
        exit(EXIT_SUCCESS);  
    }
    close(ServerIDToKillOnly);
    exit(EXIT_SUCCESS);
}

int main(int argc, char const *argv[]) {

    // Walidacja ilosci podanych argumentow:
    if (argc != 2){
        cerr << "Poprawne uzycie programu : ./server.o <port>" << endl;
        exit(EXIT_FAILURE);
    }
    //Walidacja numeru portu:
    try {
        int port = stoi(argv[1]);
        if (port <= 0 || port >= 65535) {
            throw invalid_argument("Numer portu nie zawiea sie w dozwolonym przedziale.");
        }
        if (port >= 0 && port <= 1024) {
            cout << "Zarezerwowane numery portow! Potrzebujesz roota." << endl;
        }

    } catch (const exception& e) {
        cerr << "Niepoprawnie wpisany numer portu: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }

    // Struktura przechowujaca informacje adresowe:
    struct sockaddr_in adr_ipv4={};
    adr_ipv4.sin_family = AF_INET;
    adr_ipv4.sin_port = htons(atoi(argv[1]));
    adr_ipv4.sin_addr.s_addr = htonl(INADDR_ANY);
    // deskryptor socketu serwera:
    int serverID;
    // bufor na przesylane tresci; max rozmiar wiadomosci = 256 znakow
    char buffor[256] = {};

    //tworzenie socketa tcp
    serverID = socket(PF_INET, SOCK_STREAM, 0);

    // obsluga sygnalu zabicia serwera
    signal(SIGINT, signalHandler);

    if (serverID < 0) {
        cerr << "Error while creating creating socket" << endl;
        exit(EXIT_FAILURE);
    }

    // Skojarzenie adresu serwera z gniazdem
    if((bind(serverID, (sockaddr*)&adr_ipv4, (socklen_t)sizeof(adr_ipv4))) < 0) {
        cerr << "Blad polaczenia ze strony serwera" << endl;
        perror("");
        exit(EXIT_FAILURE);
    }
    cout << "Serwer dziala na porcie: " << argv[1] << endl;

    // Nasluchiwanie na gniezdzie
    if ((listen(serverID, connectionLimit))== -1) {
        perror("Blad listen :");
        exit(EXIT_FAILURE);
    }

    ServerIDToKillOnly = serverID; 


    //Zbi�r grupy klientow
    fd_set readFdSet, fdSet;

    FD_ZERO(&fdSet);
    FD_SET(serverID, &fdSet);

    while (true) {
        readFdSet = fdSet;
        if ((select(FD_SETSIZE, &readFdSet, nullptr, nullptr, nullptr)) == -1) {
            perror("Blad select'a : ");
            exit(-1);
        }

        for (int clientSocketDesc = 0; clientSocketDesc < FD_SETSIZE; clientSocketDesc++) {
            if (FD_ISSET(clientSocketDesc, &readFdSet)) {
                if (clientSocketDesc == serverID) {
                    char ip4Addr[16] = {};
                    sockaddr_in tempAdr = {};
                    socklen_t tempAdrLen = sizeof(tempAdr);

                    int newConDesc = accept(serverID, (sockaddr*)&tempAdr, &tempAdrLen);
                    if (newConDesc == -1) {
                        perror("Blad accept'a: ");          
                    }

                    FD_SET(newConDesc, &fdSet);

                    cout << "Nowe polaczenie z : " 
                    << inet_ntop(AF_INET, &tempAdr.sin_addr, ip4Addr, sizeof(ip4Addr)) << ':'
                    << ntohs(tempAdr.sin_port) << endl << endl;
          
                } else {
                    char msg[bufor] = {};
                    int bytesSent = read(clientSocketDesc, msg, sizeof(msg));
                    if (bytesSent < 0) {
                        perror("Blad odczytu przeslanych danych: ");
                        close(clientSocketDesc);
                        exit(EXIT_FAILURE);
                    } else if (bytesSent == 2) {
                        close(clientSocketDesc);
                        FD_CLR(clientSocketDesc, &fdSet);
                    } else {
                        msg[bytesSent] = '\0';
            
                        for (int socketDescWrite = 0; socketDescWrite < FD_SETSIZE; ++socketDescWrite){

                            if (FD_ISSET(socketDescWrite, &fdSet)) {
                                if (socketDescWrite != serverID && socketDescWrite != clientSocketDesc) {
                                    if ((send(socketDescWrite, msg, bytesSent, 0)) == -1) {
                                        perror("Blad wysylania plikow!: ");
                                        exit(EXIT_FAILURE);
                                    }
                                }
                            }
                        }
                        cout << "Wiadomosc: " << msg;
                    }
                }
            }
        }
    }

    cout << "Dziekuje dobranoc..." << endl;
    close(serverID);
    return 0;
}




